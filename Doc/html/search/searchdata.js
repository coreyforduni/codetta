var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwz~",
  1: "_abcdefgijlmnopqstvw",
  2: "cjm",
  3: "abcdefgijlmnoprstvw",
  4: "abcdefghijlmnopqrstuvw~",
  5: "abcdefghiklmnopqrstuvwz",
  6: "fsu",
  7: "bcfgimno",
  8: "abcdefghiklmnopqrstuv",
  9: "_acdefghimnoprstvwz",
  10: "jm"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Pages"
};

