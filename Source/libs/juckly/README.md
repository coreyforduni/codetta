# Juckly
**Juckly is a visual programming library for creating block based applications with JUCE.**

It is inspired by (allthough not a direct port of) [Google's Blockly](https://github.com/google/blockly). 
If usage of JUCE components or C++ benefits aren't critical to your project - I reccomend you use this instead.
___
Started work 14th August 2018. 
##### Version History
**For the full version history of the Codetta project click [here](https://bitbucket.org/coreyforduni/codetta/src/master/)**
___
## Authors
* **Corey Ford** - Initial work
* **Serena Jones** - Graphics 
___
## Acknowledgments
* **Chris Nash** - Thanks for supervising the project for my undergrad dissertation.  
