/* =========================================================================================

   This is an auto-generated file: Any edits you make may be overwritten!

*/

#pragma once

namespace BinaryData
{
    extern const char*   Readme_md;
    const int            Readme_mdSize = 1027;

    extern const char*   binClosed_png;
    const int            binClosed_pngSize = 22626;

    extern const char*   binOpen_png;
    const int            binOpen_pngSize = 16495;

    extern const char*   blankGreen_png;
    const int            blankGreen_pngSize = 47685;

    extern const char*   blankRed_png;
    const int            blankRed_pngSize = 49798;

    extern const char*   demoFunctionHolder_png;
    const int            demoFunctionHolder_pngSize = 15190;

    extern const char*   demoSliderIcon_png;
    const int            demoSliderIcon_pngSize = 48038;

    extern const char*   desk_jpg;
    const int            desk_jpgSize = 256924;

    extern const char*   functionBlankBlue_png;
    const int            functionBlankBlue_pngSize = 38738;

    extern const char*   loopingdesk_jpg;
    const int            loopingdesk_jpgSize = 730198;

    extern const char*   startBlankGreen_png;
    const int            startBlankGreen_pngSize = 44720;

    extern const char*   startBlankRed_png;
    const int            startBlankRed_pngSize = 46507;

    extern const char*   README_md;
    const int            README_mdSize = 708;

    extern const char*   MusiSync_ttf;
    const int            MusiSync_ttfSize = 77604;

    extern const char*   upsideDownQuaver_png;
    const int            upsideDownQuaver_pngSize = 9108;

    extern const char*   README_md2;
    const int            README_md2Size = 940;

    extern const char*   CodettaLogo2_png;
    const int            CodettaLogo2_pngSize = 38044;

    extern const char*   acousticGuitar_png;
    const int            acousticGuitar_pngSize = 118634;

    extern const char*   altoSax_png;
    const int            altoSax_pngSize = 126135;

    extern const char*   bassoon_png;
    const int            bassoon_pngSize = 109276;

    extern const char*   cello_png;
    const int            cello_pngSize = 106987;

    extern const char*   distortionGuitar_png;
    const int            distortionGuitar_pngSize = 120664;

    extern const char*   electricBass_png;
    const int            electricBass_pngSize = 123297;

    extern const char*   flute_png;
    const int            flute_pngSize = 89252;

    extern const char*   glockenspiel_png;
    const int            glockenspiel_pngSize = 134053;

    extern const char*   pad_png;
    const int            pad_pngSize = 108471;

    extern const char*   piano_png;
    const int            piano_pngSize = 69092;

    extern const char*   tenorSax_png;
    const int            tenorSax_pngSize = 128990;

    extern const char*   trumpet_png;
    const int            trumpet_pngSize = 117495;

    extern const char*   viola_png;
    const int            viola_pngSize = 156276;

    extern const char*   violin_png;
    const int            violin_pngSize = 158060;

    extern const char*   darkPinkBlock_png;
    const int            darkPinkBlock_pngSize = 50610;

    extern const char*   dynamicsChangerIcon_png;
    const int            dynamicsChangerIcon_pngSize = 83819;

    extern const char*   bassClefBlock_png;
    const int            bassClefBlock_pngSize = 76270;

    extern const char*   honeydewBlock_png;
    const int            honeydewBlock_pngSize = 41704;

    extern const char*   codettaStartBlock_png;
    const int            codettaStartBlock_pngSize = 62077;

    extern const char*   dynamicsBlockIcon_png;
    const int            dynamicsBlockIcon_pngSize = 56874;

    extern const char*   endRepeatBlockIcon_png;
    const int            endRepeatBlockIcon_pngSize = 50908;

    extern const char*   fiveFourBlock_png;
    const int            fiveFourBlock_pngSize = 48333;

    extern const char*   fiveFourBlockIcon_png;
    const int            fiveFourBlockIcon_pngSize = 69286;

    extern const char*   fourFourBlock_png;
    const int            fourFourBlock_pngSize = 55331;

    extern const char*   fourFourBlockIcon_png;
    const int            fourFourBlockIcon_pngSize = 67916;

    extern const char*   functionBlankRed_png;
    const int            functionBlankRed_pngSize = 24453;

    extern const char*   globalTempoBlockIcon_png;
    const int            globalTempoBlockIcon_pngSize = 82923;

    extern const char*   globe_png;
    const int            globe_pngSize = 25874;

    extern const char*   orangeBlock_png;
    const int            orangeBlock_pngSize = 51313;

    extern const char*   pinkBlock_png;
    const int            pinkBlock_pngSize = 46585;

    extern const char*   startRepeatBlock_png;
    const int            startRepeatBlock_pngSize = 50645;

    extern const char*   tempoChangerIcon_png;
    const int            tempoChangerIcon_pngSize = 73437;

    extern const char*   tempoSetterIcon_png;
    const int            tempoSetterIcon_pngSize = 65400;

    extern const char*   threeFourBlock_png;
    const int            threeFourBlock_pngSize = 53535;

    extern const char*   threeFourBlockIcon_png;
    const int            threeFourBlockIcon_pngSize = 71181;

    extern const char*   trebleClefBlock_png;
    const int            trebleClefBlock_pngSize = 77335;

    extern const char*   twoFourBlock_png;
    const int            twoFourBlock_pngSize = 110023;

    extern const char*   twoFourBlockIcon_png;
    const int            twoFourBlockIcon_pngSize = 65773;

    extern const char*   yellowBlock_png;
    const int            yellowBlock_pngSize = 43361;

    extern const char*   CHANGELOG_md;
    const int            CHANGELOG_mdSize = 2718;

    extern const char*   README_md3;
    const int            README_md3Size = 1147;

    extern const char*   REFERENCES_md;
    const int            REFERENCES_mdSize = 1765;

    // Number of elements in the namedResourceList and originalFileNames arrays.
    const int namedResourceListSize = 59;

    // Points to the start of a list of resource names.
    extern const char* namedResourceList[];

    // Points to the start of a list of resource filenames.
    extern const char* originalFilenames[];

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding data and its size (or a null pointer if the name isn't found).
    const char* getNamedResource (const char* resourceNameUTF8, int& dataSizeInBytes);

    // If you provide the name of one of the binary resource variables above, this function will
    // return the corresponding original, non-mangled filename (or a null pointer if the name isn't found).
    const char* getNamedResourceOriginalFilename (const char* resourceNameUTF8);
}
