# JUCE-Soundfonts

**Forked & reworked for the Codetta project!**
___
## Information

### [Click here for the orignal repo this was forked from!](https://github.com/cpenny42/juce-soundfonts)

This project shows how to use soundfonts within JUCE. It relies on the [FluidLite Project](https://github.com/divideconcept/FluidLite), a small, cross-platform library for playing soundfonts.

This demo contains musescores sf2 file. 

Not a submodule due to filesize resctrictions.

___
## References

### None of this is my own code... credit goes to the repo reference below:
* cpenny42 (2018) *Github: juce-soundfonts* Available from: https://github.com/cpenny42/juce-soundfonts [Accessed 15 March 2019]

### The sound-font file embedded is:
* genedelisa (2014) *Github: GeneralUser GS MuseScore v1.422.sf2* Available from: https://github.com/genedelisa/MIDIPlayer/blob/master/MIDIPlayer/GeneralUser%20GS%20MuseScore%20v1.442.sf2  [Accessed 15 March 2019]

Thanks to Sam Hunt for passing this onto me!



