# MusiSync Notation Engine
**A music notation engine designed for use within [Codetta.](https://bitbucket.org/coreyforduni/codetta/src/master/)**
___
Started work 2nd October 2018.
##### Version History
**For the full version history of the Codetta project click [here](https://bitbucket.org/coreyforduni/codetta/src/master/)**
___
## Authors
* **Corey Ford** - Initial work
___
## Acknowledgments
Thanks to... 
* **Chris Nash** - Thanks for supervising the project for my undergrad dissertation.  
* **Sam Hunt** - For discussing strategies early on for programming music notation.  
* **Robert Allgeyer** for the [MusiSync Font](https://www.fontspace.com/robert-allgeyer/musisync) (SIL Open Font License).
___
## References
### The reference for the MusiSync Font used is...
* Allgeyer, R. (2005) *Fontspace: MusiSync Font*. Available from: https://www.fontspace.com/robert-allgeyer/musisync [Accessed 15 March 2019]  
