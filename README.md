# Codetta
**Codetta is a block-based application designed to aid primary educators teaching of music. **
___
## Version 1.0
Started work 14th August 2018.
##### Version History
**See complete version history [here](https://bitbucket.org/coreyforduni/codetta/src/master/CHANGELOG.md).**
___
## Getting Started
For the dissertation: you can load the app in the folder "App" on the CD.

//TODO: Download for mac
//TODO: Download for windows

**Prerequisites**
* If using the source code you will need to get JUCE from [here](https://shop.juce.com/get-juce).
___
## Contributing
//TODO: talk about this and submitting pull requests once publicly published (after uni).
___ 
## Authors
* Corey Ford - Initial work
* Serena Jones - Graphics 
___
## License
//TODO: Add license information (plan is after uni to remove 3rd party stuff)!
___
## Acknowledgments
Thanks to:
    
*  **Chris Nash** for the fantastic support supervising my undergraduate dissertation. 

*  **Sam Hunt** for providing feeback, and passing the MuseScore sound-font on to me. 

*  **cpenny32** for the [juce-soundfonts](https://github.com/cpenny42/juce-soundfonts) demo forked, forming the source code in the 3rd party folder of this application. (See more [here](). )
