# References
All references are cited in the code by a comment with the prefix // REF:
___

* Aros (2015) *Drag&Drop from a toolbar* Available from: https://forum.juce.com/t/drag-drop-from-a-toolbar/15926 [Accessed 15 March 2019]

* Ford, C. (2016) *StackOverflow: How to solve infinite loop for generated music?* Avaliable from: https://stackoverflow.com/questions/39751283/how-to-solve-infinite-loop-for-generated-music [Accessed 15 March 2019]

* Gamma, E., Helm, R., Johnson, R. and Vissides, R. (1994) *Design Patterns: Elements of Reusable Object-Oriented Software*. Boston: Addison Wesley

* Gaster, B. (2018) *Gitlab: note.hpp* Available from: https://gitlab.uwe.ac.uk/cl_audio/claudio_board/blob/master/libs/include/midi/note.hpp [Accessed 15 March 2019]

* Maloney, J., Resnick, M., Rusk, N., Silverman, B. and Eastmond, E. (2010) The Scratch Programming Language and Environment. *ACM Transactions on Computing Education* [online]. 10 (4) [Accessed 15 Nov 2018].

* thecoreyford (2018) *Github: MidiEventList.h* Available from: https://github.com/thecoreyford/Step-Sequencer/blob/master/Source/audio/MidiEventList.h [Accessed 15 March 2019]

* thecoreyford (2019) *Funky Looking App Icon* Available from: https://forum.juce.com/t/funky-looking-app-icon/31990 [Accessed 15 March 2019]

* user33033 (2013) *User Experience: Why is 1366x768 considered by some to be the standard widescreen for modern laptops?* Available from: https://ux.stackexchange.com/questions/41466/why-is-1366x768-considered-by-some-to-be-the-standard-widescreen-for-modern-lapt [Accessed 15 March 2019]

* WeAreROLI (2018) *Github: WidgetsDemo.h* Available from: https://github.com/WeAreROLI/JUCE/blob/master/examples/GUI/WidgetsDemo.h [Accessed 15 March 2019]
