# Version History
___

## Version 0.1
* **Started work 14th August 2018, all work prior to first Pilot Iterative Design Session.**

## Version 0.2
* **Initial version used for Pilot Iterative Design Session 1.**

## Version 0.3
* **Version used for ITE Iterative Design Session 1.**
* Ensured juce-soundfont submodule correctly incorporated.
* Adjusted 3/4 bar sizing.
* Added start and repeat block prototypes.
* Sound font written to cache memory on program launch. 

## Version 0.4
* **Version used for Pilot Iterative Design Session 2.**
*  Implemented a viewport with scrollable background.
*  Soundfonts now contained as Xcode Resources
*  Added browser for viewing tutorials from site.
*  Wrote introductory tutorial. 
*  Added stop/play buttons.
*  Updated fluid-synth to perform program change messages.
*  Note-off messages now part of event list cycle. 

## Version 0.5
* **Version used for ITE Iterative Design Session 2.**
*  Increase pop up menu size for notes.
*  Added ledger lines to notation engine.
*  Added bin to viewport wrapper
*  Tutorial now have a vertical layout.
*  Added clef blocks.
*  Added tempo, and start tempo setter blocks (no GUI).

## Version 0.6
* **Version used for ITE Iterative Design Session 3.**
*  Added 2/4 bar.
*  Added 5/4 bar.
*  Added tempo setter and global tempo graphics.
*  Wrote getter for the bpm value.
*  Added tempo changer block.
*  Added tutorial 2. 
*  Added extra text to MusiSync-Notation-Engines note picker, to make notes more distinquishable.

## Version 0.7
* **Version used for ITE Iterative Design Session 4.**
*  Added Info-Bar.
*  Changed tempo changer block to orange colouring.
*  Seperated playback settings into .cpp & header.
*  Extracted audio and place into its own audio class.
*  Added initialising program change messages for channels.
*  Added instrument blocks.
*  Fixed midi off notes sounding when music stops playing.
*  Added dynamics block.
*  Reorganised tab system on toolbox.
*  Added tutorial 3.

## Version 0.8
* **Version used for ITE Iterative Design Session 5.**
*  Swapped the global tempo block for a global tempo widget.
*  Added MuseScore sound-font
*  Removed juce-soundfont as submodule (file size restrictions).
*  Added Pitch tutorial
*  Play button larger and "lime" in colour. 

## Version 0.9
* **Version used for ITE Iterative Design Session 6.**
*  "Tempo Changer" block & Global Tempo widget now have "honeydew" background.
*  Added clef information to instruments, visible in info bar.
*  Moved pitch category till after dynamics in the toolbox.
*  Added Dynamics Changer Block
*  Limited velocity bounds in Playback Settings. 
___

## Version 1.0
* **Final Version Submitted for Dissertation.**
* Added new graphics from Serena Jones.
* Fixed Quaver note tails (now face correct way)
* Added academic references.
* Added new Icon

___
