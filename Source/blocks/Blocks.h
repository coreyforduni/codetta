/*
  ==============================================================================

    Blocks.h
    Created: 24 Oct 2018 1:45:20pm
    Author:  Corey Ford

  ==============================================================================
*/

#pragma once

#include "BarBlocks.h"
#include "StartBlock.h"
#include "StartRepeat.h"
#include "EndRepeat.h"
#include "ClefBlocks.h"
#include "TempoSetter.h"
#include "TempoChanger.h"
#include "Instruments.h"
#include "DynamicsBlock.h"
#include "DynamicsChanger.h"
